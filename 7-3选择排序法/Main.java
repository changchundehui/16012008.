import java.util.Scanner;
public class Main{
  public static void main(String args[]){
    Scanner reader = new Scanner(System.in);
	  int n = reader.nextInt();
	  int[] a = new int[n];       //定义数组
	  int x=0;
	  for(int i=0;i<n;i++){
	    a[i]=reader.nextInt();
	  }
	  for(int i=0;i<n;i++){
	    for(int j=1;j<n;j++){
	      if(a[j]>a[j-1]){       //前一个数和后一个数进行比较，比后一个数小，进行交换
	        x=a[j];
	        a[j]=a[j-1];
	        a[j-1]=x;
	      }
	    }
	  }
	  for(int i=0;i<n;i++){
	    System.out.print(a[i]);  
	    if(i!=n-1){
	      System.out.print(" ");
	    }
	  }
  }
}